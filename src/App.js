import React from "react";
import storeFile from "./store";
import "./App.css";
import ButtonRender from "./ButtonRender";
import { Provider } from "react-redux";
import Examples from "./Expressions";
import { applyMiddleware, createStore, combineReducers } from "redux";
import examplesReducer from "./Expressions/reducers/examples.js";
import thunkMiddleware from "redux-thunk";
import * as Redux from "redux";
import example from "./Expressions/actions/example";

const combinedReducer = combineReducers({
  examples: examplesReducer,
});

const store = createStore(combinedReducer, applyMiddleware(thunkMiddleware));

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      out: "0",
      his: [],
    };
    this.refOutput = React.createRef();
    this.increment = 0;
  }
  tapeNumber = (value) => {
    let currentValue = value;
    let output = this.refOutput.current;
    this.setState({
      out: currentValue,
      arrayExamples: [],
    });
    if (output.value === "0") {
      output.value = "";
    }
    output.value += currentValue;
  };

  mathOperation = (value) => {
    if (value.includes("+")) {
      return parseFloat(value.split("+")[0]) + parseFloat(value.split("+")[1]);
    } else if (value.includes("-")) {
      return parseFloat(value.split("-")[0]) - parseFloat(value.split("-")[1]);
    } else if (value.includes("*")) {
      return parseFloat(value.split("*")[0]) * parseFloat(value.split("*")[1]);
    } else if (value.includes("/")) {
      if (value.split("/")[1] === "0") {
        return "Невірне значення";
      } else {
        return (
          parseFloat(value.split("/")[0]) / parseFloat(value.split("/")[1])
        );
      }
    } else {
      return value;
    }
  };

  tapeOperation = (value) => {
    let output = this.refOutput.current;
    try {
      if (value === "CE") {
        if (output.value.length === 1) {
          output.value = "0";
        } else {
          output.value = output.value.substring(0, output.value.length - 1);
        }
      } else if (value === "C") {
        output.value = "0";
      } else if (value === "=") {
        if (storeFile.operations.includes(output.value.slice(-1))) {
        } else {
          this.addToHistory(output);
          output.value = this.mathOperation(output.value);
        }
      } else {
        this.checkSymbol(value);
      }
    } catch {
      output.value = "Невірне значення";
      setTimeout(() => {
        output.value = "0";
      }, 1000);
    }
  };

  checkSymbol = (value) => {
    var output = this.refOutput.current;
    if (storeFile.operations.includes(output.value.slice(-1))) {
      output.value = output.value.substring(0, output.value.length - 1) + value;
    } else {
      console.log(storeFile.operations.includes(output.value.slice(-1)));
      if (this.increment !== 1) {
        this.tapeNumber(value);
        this.increment = this.increment + 1;
      } else {
        {
          this.addToHistory(output);
          output.value = this.mathOperation(output.value) + value;
          this.increment = 1;
        }
      }
    }
  };

  addToHistory = (output) => {
    let currentValue = output.value;
    this.setState({
      his: [
        ...this.state.his,
        currentValue + "=" + this.mathOperation(output.value),
      ],
    });
  };

  getArrayFromBack = (array) => {
    let arr = this.calculateBAck(array.arr);
    console.log(array.arr);
    console.log(this.state.his);
    console.log(array.arr.concat(this.state.his));
    this.setState({
      his: this.state.his.concat(arr),
    });
  };

  calculateBAck = (array) => {
    let arr = [];
    array.map((index) => arr.push(index + "=" + this.mathOperation(index)));
    return arr;
  };

  render() {
    return (
      <div>
        <div className="container">
          <div className="output">
            <div>{this.state.test}</div>
            <input
              ref={this.refOutput}
              type="text"
              defaultValue={this.state.out}
            />
          </div>
          <ButtonRender
            tapeOperationHandler={this.tapeOperation}
            tapeNumberHandler={this.tapeNumber}
            state={this.state}
          ></ButtonRender>
        </div>
        <Provider store={store}>
          <Examples getArrayfromBackHandelr={this.getArrayFromBack} />
        </Provider>
      </div>
    );
  }
}

export default App;
