import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import exampleActions from "../actions/example";
import examples from "../reducers/examples";

class Examples extends React.Component {
  
  render() {
    const { actionFetchExamples } = this.props;
    this.props.getArrayfromBackHandelr(this.props.examples)

    return (
      <div>
        <button onClick={() => actionFetchExamples()}>
          Press me, and I will get example
        </button>

      </div>
    );
  }
}

const mapReduxStateToProps = (reduxState) => ({ ...reduxState });
const mapDispatchToProps = (dispatch) => {
  const { fetchExamples } = bindActionCreators(exampleActions, dispatch);
  return {
    actionFetchExamples: fetchExamples,
  };
};

export default connect(mapReduxStateToProps, mapDispatchToProps)(Examples);
