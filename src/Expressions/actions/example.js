const receiveExample = (examples) => ({
  examples,
  type: "RECEIVE_EXAMPLES",
});

const requestExamples = () => ({
  type: "REQUEST_EXAMPLES",
});

const errorReceiveExamples = () => ({
  type: "ERROR_RECEIVE_EXAMPLES",
});

const getExamples = () =>
  new Promise((onSuccess) => {
    setTimeout(
      () =>
        onSuccess(
          Array.from(["2+2", "1-1", "9/3", "10*2"]).map((index) => index)
        ),

      1000
    );
  });
const fetchExamples = () => (dispatch) => {
  dispatch(requestExamples());
  return getExamples()
    .then((example) => dispatch(receiveExample(example)))
    .catch(() => dispatch(errorReceiveExamples()));
};

export default {
  fetchExamples,
};
