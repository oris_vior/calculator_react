const initialState = {
  isLoading: false,
  isError: false,
  arr: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "REQUEST_EXAMPLES": {
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    }
    case "RECEIVE_EXAMPLES": {
      const { examples } = action;
      return {
        ...state,
        isLoading: false,
        arr: examples,
      };
    }
    case "ERROR_RECEIVE_EXAMPLES": {
      return {
        ...state,
        isError: true,
      };
    }
    default:
      return state;
  }
};
