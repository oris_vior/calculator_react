import { Button } from "../node_modules/@mui/material/index";
import store from "./store";

const ButtonRender = (props) => {
  return (
    <div className="button">
      {store.buttons.map((item, index) => {
        return (
          <Button
            key={index}
            onClick={() => props.tapeNumberHandler(item.val)}
            variant="outlined"
          >
            {item.val}
          </Button>
        );
      })}
      {store.operations.map((item, index) => (
        <Button
          key={index}
          onClick={() => props.tapeOperationHandler(item)}
          variant="outlined"
        >
          {item}
        </Button>
      ))}
      <div className="history">
        {props.state.his
          .filter(
            (item) => item !== props.state.his[props.state.his.length - 1]
          )
          .map((item, index) => (
            <p key={index}>{item}</p>
          ))}
      </div>
      <div className="lastElement">
        <p key={`index`}>{props.state.his[props.state.his.length - 1]}</p>
      </div>
    </div>
  );
};
export default ButtonRender;
